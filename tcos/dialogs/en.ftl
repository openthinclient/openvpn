login-title = OpenVPN Login

connection-lost-title = OpenVPN connection lost

connection-lost = The connection to the OpenVPN server was lost.

    The OpenVPN connection was either disconnected or could not be established.

connection-lost-check-login = This can have many causes. Maybe the username/password was entered incorrectly.

connection-lost-ask-reconnect = Reconnect?

connection-lost-confirm = Reconnect


ask-disconnect-title = Disconnect OpenVPN connection

ask-disconnect = Do you want to disconnect the connection?

ask-disconnect-confirm = Disconnect


# ----------------------------------------------------------------------------
# Balloon messages
# ----------------------------------------------------------------------------

status-title = OpenVPN

status-connecting = Establishing connection...

    This can take up to 60 seconds.

status-connected = Connection established successfully.

status-disconnected = Connection closed.


# ----------------------------------------------------------------------------
# Configuration error messages
# ----------------------------------------------------------------------------

configuration-error-title = OpenVPN configuration error

no_config_file = No configuration file specified.

    Please check the configuration.

config_file_not_found = Configuration file not found.

    Please check the configuration.


# ----------------------------------------------------------------------------
# Error messages
# ----------------------------------------------------------------------------

error-title = OpenVPN Error

error_uncaught = An unexpected error occurred.

    {$error}

    Please contact your administrator.
