login-title = OpenVPN Login

connection-lost-title = OpenVPN Verbindung unterbrochen

connection-lost = Die Verbindung zum OpenVPN-Server wurde unterbrochen.

    Die OpenVPN Verbindung wurde entweder getrennt oder konnte nicht aufgebaut werden.

connection-lost-check-login = Dies kann viele Ursachen haben. Eventuell wurden Benutzername/Passwort falsch angegeben.

connection-lost-ask-reconnect = Verbindung erneut herstellen?

connection-lost-confirm = Verbinden


ask-disconnect-title = OpenVPN Verbindung trennen

ask-disconnect = Möchten Sie die Verbindung trennen?

ask-disconnect-confirm = Trennen


# ----------------------------------------------------------------------------
# Balloon messages
# ----------------------------------------------------------------------------

status-title = OpenVPN

status-connecting = Verbindung wird hergestellt...

    Dies kann bis zu 60 Sekunden dauern.

status-connected = Verbindung erfolgreich hergestellt.

status-disconnected = Verbindung getrennt.


# ----------------------------------------------------------------------------
# Configuration error messages
# ----------------------------------------------------------------------------

configuration-error-title = OpenVPN Konfigurationsfehler

no_config_file = Keine Konfigurationsdatei angegeben.

    Bitte überprüfen Sie die Konfiguration.

config_file_not_found = Konfigurationsdatei nicht gefunden.

    Bitte überprüfen Sie die Konfiguration.


# ----------------------------------------------------------------------------
# Error messages
# ----------------------------------------------------------------------------

error-title = OpenVPN Fehler

error_uncaught = Ein unerwarteter Fehler ist aufgetreten.

    {$error}

    Bitte informieren Sie Ihren Administrator.
